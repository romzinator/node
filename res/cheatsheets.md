# Cheatsheets cours nodejs

## Commandes utiles

Initialisation projet node:

`npm init`

Installation d'un packet node:

`npm install <package_name> --save` (le --save sert à enregistrer la dépendance dans le fichier package.json)

Installer les dépendence du projet:

`npm install`

Exécution script:

`node <fichier>.js`

Installation dépendance JS (front)

`bower install <lib>`

## Nodejs

Importer un module:

```javascript
var lib = require('<lib_name>');

lib.fonction();
```

Exporter un module

```javascript

module.exports = {
  fonction: function() {
      console.log("TODO");
  }
};
```

Utilisation du filesystem

```javascript
const fs = require("fs");

const currentDirectory = process.cwd();


// Readfile callback version
fs.readFile("./file.txt", "utf8", (err, result) =>{
  if(result) {
    console.log(result);
  }else {
    console.error(err);
  }
});

//Readfile synchrone

const data = fs.readFileSync("./text.txt", "utf8");

Writefile
fs.writeFileSync("./test.txt", "coucou", "UTF8");
```

Creation d'un objet promise

```javascript
const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("Data");
  });
});
```

Utilisation des promises

```javascript
promise.then((data) => {
  console.log(data);
}).catch((err) => {
  console.error(err);
});
```

Utilisation de await / asynch
```javascript
class MaClass {
  async testAwait() {
    try {
      var data = await promise; // Attend le resolve de l'obj Promise
    }catch(e) {
      console.error(e); // promise rejeté
    }
  }
}

const obj = new MaClass();

obj.testAwait(); // Cette method sera appelé en asynchrone
```

## Minimist

Installation:
`npm install minimist`

```javascript

var argv = require('minimist')(process.argv.slice(2));
console.log(argv);
```

## Chalk

Installation

`npm install chalk --save`

Utilisation de chalk

```javascript
const c = require("chalk");

console.log(c.red("Text en rouge"));
```

## Mongoose

Doc: [https://mongoosejs.com/docs/guides.html](https://mongoosejs.com/docs/guides.html)

Connection

```javascript
const database = "mydb";
const url = `mongodb://localhost:27017/${database}`;

mongoose.connect(url, {useNewUrlParser:  true, poolSize: 10, useUnifiedTopology: true, connectTimeoutMS: 3000, keepAlive: 1 });
```

Schema

```javascript
const schema = new mongoose.Schema({
  firstname: String, // Type string
  lastname: String,
  age: {type: Number, min: 18}, // Type number with min value
  skills: [], // Type array
  address: { // Type nested
    street: { type: String },
    zip: { type: String },
    city: { type: String },
    country: { type: String },
  },
},
{
  timestamps: true // Add createdAt and updated at
});

schema.methods.fullName = function() { // Create schema methods
  return `${this.firstname} ${this.lastname}`;
};

schema.statics.findBySkills = function(skills = []) { // Create global methods
  return this.find({skills: { // Search elements
    $all: skills
  }})
};

if(db.models.Customer) { // Prevent duplicate model creation
  return db.models.Customer;
}else {
  return db.model("Customer", schema);
}
```

Operations

```javascript

// Creation

let model = new Customer({firstname : "test", lastname: "Test"});
model.age = 5;
model.skills = ["CSS", "JS"];

await model.save(); // Persist data to database

model.delete(); // Delete data entry

```

## Inquirer

Installation

`npm install inquirer --save`

Inquier permet de créer des formulaire interactifs

```javascript
const inquirer = require("inquirer");

const questions = [
  {
    name: "nom",
    type: "input",
    message :"Votre nom",
    validate :(value) => {
      return value.length > 0 ? true : "Vous devez renseigner un nom"
    }
  },
  {
    name: "sure",
    type:"confirm",
    message: "Are you sure?",
  },
  {
    name: "skills",
    type:"checkbox",
    message: "Vos skills",
    choices: ["CSS", "HTML", "JS"]
  }
];

inquirer.prompt(questions).then((response) => {
  console.log(response);
});
```

## Utilisation de Axios

Axios permet de faire des appels http

Installation:

`npm install axios --save`

Utilisation:

```javascript
const axios = require('axios');

// Get request
axios.get('<url>').then((response) => {
  console.log(response);
}).catch((err) => {
  console.error(err);
});

// Post request with datas

axios.post('<url>', {username: 'test', password:'test'})
.then((result) => {
  console.log(result);
});

```

## Gitlab api

Vous pourrez trouver la doc d'utilisation ici :
- [Issues api](https://docs.gitlab.com/ee/api/issues.html)
- [Issues stats api](https://docs.gitlab.com/ee/api/issues_statistics.html)

L'API giltab est une api rest, il suffit de faire des requêtes HTTP en GET.
Voici un exemple d'appel:

https://gitlab.com/api/v4/projects/18107460/issues?state=opened&access_token=<access_token>

## Express

Installation:

`npm install express --save`

Utilisation:

```javascript
const express = require('express');

var app = express();

// Rend le répertoire public accessible à la racine du site
app.use(express.static(__dirname + '/public'));

// Ajout du repertoire bower_components

app.use('/bower_components',  express.static(__dirname + '/bower_components'));

// Creation des routes
app.get('/', function(req, res) { // Chemin root
  res.send('hello world'); // callback appelé quand on va sur http://<host>:<port>/
});

app.get('/uri', function(req, res) { // Chemin root
  res.send('hello world 2'); // callback appelé quand on va sur http://<host>:<port>/uri
});

// Route dynamique
app.get('/uri/:variable/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Params: ' + req.params.variable);
});

app.listen(3000, function () { // Ecoute sur le port 3000
  console.log('listening on port 3000!');
});
```

## EJS

Installation:
`npm install ejs --save`

Utilisation avec express:

Les fichiers templates doivent être mis dans un répertoire "views"

Javascript:

```javascript
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.render("index", {data: "Test"});
});
```

views/index.ejs:

```html
<div>
 <span><%= data %></span>
 <%= include('partial', {data: "test"})%>
 <% for(el in arr) { %>
   <%= el %>
 <% } %>
</div>
```

## Morris JS

Installation:
```HTML
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
```

Bar:

```javascript
var chart = new Morris.Bar({
  // ID of the element in which to draw the chart.
  element: 'test-chart',

  data: [
    {date: '10/10/2020', axe1: 3, axe2: 2},
    {date: '11/10/2020', axe1: 2, axe2: null},
    {date: '12/10/2020', axe1: 0, axe2: 2},
    {date: '13/10/2020', axe1: 2, axe2: 4}
  ],
  xkey: 'date',
  ykeys: ['axe1', 'axe2'],
  labels: ['Axe1', 'Axe 2']
});

//Mise à jour :

chart.setData([
  {date: '10/10/2020', axe1: 3, axe2: 2},
  {date: '11/10/2020', axe1: 2, axe2: null},
  {date: '12/10/2020', axe1: 0, axe2: 2},
  {date: '13/10/2020', axe1: 2, axe2: 4},
  {date: '14/10/2020', axe1: 3, axe2: 5}
]);
```

Donut:

```javascript
var donut = Morris.Donut({
  element: 'donut-chart',
  data: [
    {value: 70, label: 'foo'},
    {value: 14, label: 'bar'},
    {value: 16, label: 'baz'},
  ],
});

```

## Socket.io

Installation:

`npm install socket.io`

Utilisation Serveur

```javascript
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/sendall', (req, res) => {
  io.sockets.emit('server_message_everyone', {data: "Hello everyone"});
  res.redirect('back');
});

io.on('connection', (socket) => {
  socket.on('client_message', (data) => { // Serveur ecoute les events du client
    socket.emit('server_message', { data: "hello"}); // Le serveur envois un message au client
    socket.broadcast.emit('server_message_everyone', {data: "Hello everyone"}); // Le serveur envois un message a tout le monde sauf le client
  });

  socket.on('disconnect', () => {

  });
  console.log('a user connected');
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});
```

Utilisation Client

```javascript
//const socket = io.connect('http://localhost:3000');
const socket = io.connect();

socket.on('custom_event', function(data) {
  console.log(data);
});

socket.emit("message", { message: "salut"});

socket.on('message', function(data) {
  console.log(data);
});

```

Utilisation des rooms

```javascript

io.on('connection', function(socket){
  socket.join('some room'); // join a room

  socket.broadcast.to('some room').emit('some event')

  socket.leave('some room'); // leave a room
});

io.to('some room').emit('some event', "Hello"); // broadcast everyone in room

// Send message to specific user
// id = socket id
socket.broadcast.to(id).emit('my message', msg);
```
