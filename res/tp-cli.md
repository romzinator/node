# Gitlab Cli Library

Le but de ce TP est de créer une librairie Gitlab pour interagir avec l'api Gitlab et récupérer des informations.
Le projet devra:
- Avoir un fichier index.js qui sera le point d'entré du programme
- Un fichier libs/gitlab.js qui contiendra une class pour intéragir avec l'API
- Un fichier de configuration JSON qui contiendra les informations:
  - api_key
  - project_id
  - url
- Les différentes méthodes devront pouvoir être appelées indépendemment en passant un argument à votre programme (node index.js stats) par exemple
- Proposer une aide d'utilisation intuitive si on ne met pas d'aguments ou que l'on passe "help" en argument (Utilisez chalk pour faire une aide stylée)
- Créer une commande "init" qui permettra de générer le fichier de configuration JSON (inquirer)
  - Utilisez l'url https://gitlab.com/api/v4/ comme valeur par defaut pour le champs url

## Fonctions gitlab à créer
- listRecentIssues => Liste les 20 dernier issues
- issuesStats => Récupère le nombre d'issue ouvertes et fermées. Ajouter une plage de date en paramètre pour filtrer sur la date de création des issues.
- listClosedIssues => Retourne la liste des issues en état closed. Ajouter une plage de date en paramètre pour filtrer sur la date de création des issues.
- listOpenedIssues => Retourne la liste des issues en état opened. Ajouter une plage de date en paramètre pour filtrer sur la date de création des issues.
- averageOpenTime => Prend en paramètre une date de début et une date de fin. Récupérez les tickets dons la date de création se trouve dans la plage de date passé en paramètre.  Retourne le temps moyen d'ouverture d'un ticket (entre le created_at et le closed_at). Il faudra retourner le nombre de jours d'ouverture moyen.


## Création des identifiants gitlab
- Connectez vous à Gitlab et allez sur "Profile"
- Dans le menu de gauche, cliquez sur "Access Tokens"
- Ajoutez un nom et mettez les droits "api"
- Stocker le token dans un coin (on ne peut pas le récupérer une fois généré, si vous le perdez il faudra supprimer le token et en recréer un)

## Création du projet

Pour faire le TP de niveau epic, ne cliquez pas sur le lien ci-dessous et essayez de le faire tout seul (ou par équipe de 2 max).
Si vous ne vous sentez pas à l'aise pour faire ce TP sans aide, cliquez sur le lien ci-dessous.

<details>
<summary>TP guidé cliquez ici</summary>

- Créez le repertoire de votre projet
- Allez à l'intérieur et initialisez le projet node (npm init)
- Si vous comptez versionner votre projet avec git, faite un fichier .gitignore et excluez au moins les répertoires node_modules et bower_components
- Créez un répertoire libs et un fichier gitlab.js dedans
- Créez une classe Gitlab qui prendra en constructeur l'api key, l'url et l'id du projet Gitlab, et stockez les en attributs de la classe
- Créez une méthode "listRecentIssues" qui devra retourner la liste des issues dont le status est "ouvert" (pour l'instant retournez un tableau vide)
- Exportez votre classe
- Créez un fichier index.js et importez la classe que vous venez de créer, et renseignez les valeurs du constructeur en dur
- Instanciez un objet de la classe et essayez de faire un console.log sur la methode que vous venez de créer. Cela devrait afficher un tableau vide
- Retournez dans la librairie, et importer axios
- Créez les méthodes demandées en utilisant axios (vous pouvez retourner directement la promise de axios)
- Dans le fichier index.js, récupérer les arguments (avec minimist) et appeler les méthodes gitlab en rapport.
- Testez toutes les commandes
- Créer un fichier config.json comme spécifié dans l'énoncé.
- Dans index.js, lisez les informations du fichier, et changé l'appel de votre constructeur avec les valeurs dans le fichier de config plutôt qu'en dur.
- Testez que toutes les commandes fonctionnent bien
- Créez le formulaire inquirer pour demander les informations du fichier de configuration
- Quand le paramètre "init" est appelé, afficher le formulaire
- Quand le formulaire est remplis, créer le fichier config.json avec les valeurs dedans
</details>
