# Gitlab Realtime

Le but de ce TP est de mettre à jour l'interface développé avec Express en temps réél avec des nouvelles données quand une action se passe sur Gitlab.
Gitlab utilise un système de WebHooks que nous allons simuler.
Pour un soucis de simplicité, nous allons uniquement mettre à jour la liste des issues récentes (car pas de filtres date).

Pour cela il faudra donc:

- Installer et initialiser socket.io
- Créer une nouvelle route dans express qui va refaire l'appel d'api et broadcast le résultat sur tous les sockets
- Récupérer les données des sockets en front pour mettre à jour la liste des issues récentes

Pour le développement, mettez à jour la liste avec des données en dure et pas de vrais appels à Gitlab pour gagner du temps.
