# Gitlab Data visualisation

Le but de ce TP est d'utiliser la librairie que vous avez développé pour créer une interface web.
Cette interface devra montrer différents graphiques:


- Un camembert pour afficher la répartition des issues ouvertes / fermés (fonction issuesStats)
- La repartition par personne du nombre d'issues closed (Diagramme Bar)
- Affichage des issues récentes dans une liste
- Le temps moyen d'ouverture des tickets (Juste un nombre)
- Le nombre d'issue ouvert par jour (Diagramme Bar)

La page devra contenir 2 champs dates pour filtrer les graphiques (les champs seront globaux pour tous les graphiques).

<details>
<summary>TP guidé cliquez ici</summary>

- Installez express et ejs dans votre projet
- Créez un fichier server.js à la racine de votre projet
- Créez un répertoire views et ajoutez un fichier index.ejs
- Créez un répertoire "public"
- Importez la librairie Gitlab que vous avez créé
- Initialisez express.js
  - Importez la librairie
  - Initialisez la route root et affichez le template index
  - Ajoutez le répertoire public dans express
- Créez un fichier script.js dans public
- Importez le fichier script.js dans votre fichier index.ejs
- Créez les différents graphiques avec des données en dur
- Dans le callback de la route, appelez les fonctions Gitlab, et stockez les valeurs dans un objet JSON
- Passez les resultats des appels à Gitlab dans le render pour le passer au template EJS
- Créez une variable javascript dans le template avec les valeurs récupérées de Gitlab
- Changez les valeurs en dur par les vrais données
- Créez un formulaire en GET, avec comme action '/'
- Dans ce formulaire, ajoutez 2 champs dates
- Dans server.js, récupérez les paramètres pour filtrer les valeurs dans les appels à Gitlab
</details>
