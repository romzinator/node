# Introduction Nodejs

## Base de Nodejs (partie 1)

- Initialisation du projet
- Faire un console.log
- Ajouter un fichier js
- Exporter une fonction (hello world)
- Installer une librairie (chalk)
- Les tableaux
- Les objet JSON
- Les boucles
- Différence entre let / var / const
- Montrer comment récupérer des argument (process.argv)
- Montrer comment utiliser minimist
- `var argv = require('minimist')(process.argv.slice(2));`

### Exercice: Lipsum generator:
- Récupérez en argument le nombre de paragraphe à générer, et afficher dans la console
- Ajoutez des limites et messages d'erreur sur l'argument
- Bonus: Mettre une couleur random sur chaque paragraphe
- Bonus 2: Faire un vrai système de random sur les mots plutôt que sur les paragraphes

## Base Nodejs (partie 2)
- Utilisation des classes
- Dates
- Methodes utiles de chaine
- Les promises
- async / await

### Exercice: Date
- Créez une classe qui hérite de la classe "Date"
- Créez une nouvelle méthod 'format' qui prend en paramêtre un format de date et qui retourn la date formattée
- Formats:
  - Y: année
  - m: mois (Attention la méthode getMonth commence à 0 et pas 1)
  - d: jour
  - H: heure
  - i: minutes
  - s: secondes
- Exemple date.format("d/m/Y H:i:s"); // Doit retourner par exemple 14/01/2020 12:15:17
- Bonus: Rajoutez de nouveau modifs de remplacement
- Bonus: Rajouter la possibilité de mettre les mois / jours en Français (ex: "Jeudi 15 Octobre")

<details>
  <summary>Besoin d'aide?</summary>

  - Utilisez la méthode `'str'.split('')` pour récupérer chaque caractère
  - Utilisez la fonction 'map' pour traiter chaque caractère
  - Utilisez la méthode `join` pour reformer la chaine
</details>

## Prompteur

- Utilisation de readline
- Utilisation avec inquirer
  - Montrer les différents types
  - Montrer avec await et async

## File system
- Utilisation de fs
- Récupérer le chemin courant (process.cwd())
- Lire un fichier
  - En asynchrone
  - En synchrone
- Ecrire un fichier

## Mongodb
- Utilisation de mongoose
- Les modèles
- Les plugins

### Exercice: censure generator:
- Le programme doit récupérer un chemin en argument
- Tester le chemin, si il n'existe pas afficher une erreur
- Utiliser inquirer pour demander une liste de mot à censurer dans un fichier (separré par des virgules ou espaces)
- Proposer une liste de chaines de replacement "####, "****", "-----"
- Demander le nom du nouveau fichier
- Lire le fichier passé en paramètre
- Remplacer les occurences par la chaine choisis
- Ecrire le nouveau fichier
- Bonus
  - Stocker les mots dans une base mongodb
  - Créer une commande pour ajouter des mot (par exemple node lipsum.js add monMot)

## Exécuter une commande
- Montrer childprocess
- Les variables d'environnements

## Utilisation d'axios
- Faire une requete HTTP en GET / POST
- Utilisation des params

# Mini projet Gitlab: Library
[Cliquez ici pour accéder au mini projet](res/tp-cli.md)

## Express JS Part 1
- Création d'un serveur
- Création d'une route
- Création d'une route dynamique
- Récupération des datas GET / POST etc
- Reponse
  - Statuscode
  - body
- Redirection
- Système de templating
- Ajouter les chemins statiques
- Utilisation d'un system de template

## [API] Livre de recettes

- Créer une api REST pour gérer un livre de recettes en utilisant Express et Mongodb.
- Créer les différentes route pour la création du CRUD
  - recette.json en GET: Liste les recettes
  - recette.json en POST: Créer une nouvelle recette
  - recette/id.json en GET: Récupère une recette avec son id
  - recette/id.json en PUT: Met à jour la recette
  - recette/id.json en DELETE: Supprime la recette
  - recette/id/comment.json en POST: Ajoute un commentaire dans la recette
- Une recette contient:
  - Nom
  - Temps de préparation
  - Temps de cuisson
  - Liste d'ingrédients
    - Nom
    - Quantité
    - Unité (g / ml etc)
  - Liste d'étapes
    - Nom
    - Description
    - Temps
  - Commentaires
    - Nom utilisateur
    - Commentaire
- Bonus1 : Créer une nouvelle route recette/search.json pour rechercher une recette par noms ou ingrédients
- Bonus2 : Créer une interface pour afficher les recette et une page pour créer des recettes en utilisant l'API

## Exercice: Group generator
- La page doit contenir:
  - Un champs texte pour ajouter le nom d'une personne et un bouton "Ajouter"
  - La liste des noms ajoutés
  - Un champs texte "Nombre de personne max" et un bouton "Générer" pour générer les groupes aléatoirement
  - Un bouton "Rafraichir" pour effacer la liste
- Les actions (générer, ajouter, rafraichir) devront se faire via des méthodes POST
- La génération doit rediriger vers une nouvelle avec la sélection des groupes
- Pour simplifier, vous pouvez utiliser le package node "shuffle-array"

## Express JS Part 2
- Les middlewares
- Session
- Les routers
- middlewares
  - compression
  - cookie-parser
  - cookie-session
  - serve-favicon
  - csrf

## Mini projet Gitlab: Interface

[Cliquez ici pour accéder au mini projet](res/tp-express.md)

## Socket.io
- Installation
- Création serveur
- Evénement connect
- Ecouter un événement
- Envoyer un événement
- Broadcast

## Livre de recette suite

- Reprendre l'API créé dans la partie précédente.
- Si vous n'avez pas fait le bonus, créer une interface pour lister les dernières recettes (les plus récentes) et ajouter une recette.
- Créer un socket pour pouvoir mettre à jour en temps réèl la liste quand une nouvelle recette est ajoutée.
- Ajouter une page pour voir une recette.
- Afficher la liste des commentaires de la recette et ajoutez un formulaire pour pouvoir ajouter un nouveaux commentaire.
- Lors de l'ajout d'un commentaire, rafraichir en temps réèl la liste des commentaires

## VoteJS

Créer un système de vote en temps réél.
- Le sujet du vote ainsi que les reponses possibles devront être stockés dans un fichier JSON et charger au lancement du serveur
- Afficher une page web avec l'intitulé du vote ainsi que les réponses possibles et le résultat du vote de chaque réponse.
- Si l'utilisateur n'a pas encore voté, les réponses doivent être cliquable et envoyer la réponse choisis au serveur.
- Une fois que l'utilisateur a voté, on ne peut plus voter (utilisateur = socket pour que ce soit simple)
- Le résultat du vote doit se rafraichir en temps réèl dès qu'un utilisateur vote
- Bonus : faire une progressbar avec le pourcentage de chaque responses
- Bonus2: Faire une page pour choisir un nom d'utilisateur avant le vote, et vérifier que la personne n'a pas encore voté
- Bonus3: Stocker les votes / sondage dans une base mongo

## Mini projet: Gitlab realtime

[Cliquez ici pour accéder au mini projet](res/tp-socket.md)

## Mini projet: Dacti contest

Le but du jeu est d'écrire le plus rapidement possible des mots ou phrases aléatoire.


### Déroulé du jeu

- Quand on arrive sur la page, on a le choix de créer ou rejoindre une partie
- Création d'une partie
  - Choisir un nom de room
  - Choisir un nom de user
  - Le bouton OK crée la room
  - Une fois qu'on a cliqué sur le bouton, on est redirigé sur une page "Salle d'attente"
- Salle d'attente
  - La personne qui a créé la room est "l'admin" de la room, il peut choisir de démarrer la partie
  - Quand une personne se connecte à la room, on l'affiche dans la liste des joueurs
  - Quand l'admin clique sur "Démarrer" le jeu se lance
- Partie
  - Quand la partie est lancée, le serveur affiche un mot ou une phrase aléatoirement parmi une liste prédéfini (Liste dans mongodb si possible)
  - Chaque joueur doit rentrer le mot dans un champs texte et l'envoyer au serveur
  - Le serveur stock le temps de la réponse si le mot ou la phrase est exacte
  - Si le mot ou la phrase est faux à la validation, on vide le champs texte
  - Après 10 mots (ou plus c'est vous qui voyez), la partie se termine et on calcule le temps moyen de réponse de chaque joueur.
  - On affiche le classement.
  - On peut appuyer sur un bouton rejouer pour relancer une partie
- Bonus
  - Stocker les stats des joueurs dans mongodb
  - Faire des stats de temps par mot / phrase


## Mini projet Cadre photo de grand mère

Le but de ce projet est de créer une application web fullscreen qui permettra de faire défiler des galeries de photos.

L'application sera composée:
- D'une interface cliente qui s'occupera d'afficher les photos, ainsi que les notifications.
- D'une interface d'administration qui permettra de gérer les galeries photos, d'envoyer des notifications (message pour la grand mère avec si possible la possibilité de mettre une photo personnalisé avec la notification).

**Gestions des galeries:**
- Dans un premier temps, une galerie sera juste un repertoire dans le projet. Vous pourrez mettre vos images via SFTP ou autre. Il faudra lister toutes les images de la galerie et l'envoyer au client.

**Interface d'administration**
- Gestion des galeries: Spécifier le nom du répertoire à afficher. Lors d'un changement de galerie, le server devra envoyer un événement en socket pour spécifier au client que la galerie à changer et envoyer un tableau avec les nouvelles urls
- Gestion des notifications: Cette page servira à envoyer des messages au client via des sockets. La notification devra contenir un temps de pause pour gérer le temps que le message restera affiché
- Bonus1: Pouvoir sélectionner une image en même temps que la notification pour afficher la notification avec cette image  
- Bonus2: Gérer les galeries depuis l'interface d'admin: Création de repertoire + upload de photos
- Bonus3: Pouvoir programmer des notifications à l'avance

**Interface cliente**
- L'interface cliente doit récupérer la liste des photos depuis le server, et écouter les changement pour regénérer le carousel.
- L'interface cliente doit écouter sur les événements notification pour afficher une infobulle avec le message personnalisé. Si possible un signal sonore devra être envoyer
- Si une photo est associé à la notification, avancer le carousel directement à la photo indiqué et faire une pause pendant le temps spécifié
